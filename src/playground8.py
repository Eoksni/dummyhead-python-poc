import os
from dpp_calibrate_manager import DppCalibrateManager
import dpp_logging

from dpp_direction import DppDirection
from dpp_input import DppInput
from dpp_graph_setup import graph_setup
from dpp_graph4 import DppGraph4
from dpp_beep import beep
import dpp_config

dpp_logging.devLogging()
log = dpp_logging.getLogger(__name__)
config = dpp_config.setup()

beep()
graph_setup()

with DppInput(port=config['port']) as inp:
    index_of_sensor_to_fetch = 1
    index_of_sensor_to_fetch2 = 3
    graph = DppGraph4(
        config, title=f"Sensor index {index_of_sensor_to_fetch} x-direction")
    graph2 = DppGraph4(
        config, title=f"Sensor index {index_of_sensor_to_fetch2} x-direction")
    calibrator = DppCalibrateManager(
        config, os.path.join(__file__, '../..', 'temp'))
    calibrator.initiate()
    direction = DppDirection(config)
    while True:
        res = inp.read()
        calibrated = calibrator.transform(res)
        dirs = direction.transform(calibrated)
        graph.append(dirs[index_of_sensor_to_fetch])
        graph2.append(dirs[index_of_sensor_to_fetch2])

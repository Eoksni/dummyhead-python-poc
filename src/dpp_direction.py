import numpy as np
from dpp_logging import getLogger
from dpp_utils import apply_quaternion_to_vector

log = getLogger(__name__)


class DppDirection:
    def __init__(self, config):
        self.number_of_sensors = config['number_of_sensors']
        self.number_of_variables = 4

    # That pattern probably should be extracted as an interface/base class of some sort.
    def transform(self, data):
        px = np.repeat([[0, 1, 0, 0]], self.number_of_sensors, axis=0)
        pxprime = apply_quaternion_to_vector(data, px)
        return pxprime

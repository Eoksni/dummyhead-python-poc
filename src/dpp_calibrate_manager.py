import time

from dpp_calibrate import DppCalibrate
from dpp_data_collector import DppCalibrateCollector
from dpp_logging import getLogger
from dpp_utils import model2raw, raw2model

log = getLogger(__name__)


def collected_raw2model(data):
    return {k: list(map(raw2model, v)) for k, v in data.items()}


class DppCalibrateManager:
    def __init__(self, config, path_dir, on_elapsed_cb=None):
        self.config = config
        self.window = config['calibrate_window']
        self.initial_delay = config['calibrate_initial_delay']
        self.delay = config['calibrate_delay']
        self.delay_next_milestone = self.initial_delay
        self.last_milestone = 0
        self.collector = DppCalibrateCollector(config, path_dir)
        self.calibrate: DppCalibrate = None
        self.elapsed_triggered = False
        self.on_elapsed_cb = on_elapsed_cb

    def initiate(self):
        self.started = time.monotonic()
        self.last_milestone = self.started
        log.important(
            f'calibration started! now we wait {self.delay_next_milestone} ms before treating data for calibration {self.collector.current_label()}')

    def is_elapsed(self):
        elapsed = (time.monotonic() - self.last_milestone) * \
            1000 >= self.delay_next_milestone
        if elapsed and not self.elapsed_triggered:
            self.elapsed_triggered = True
            log.urgent(
                f'starting collecting calibration data for {self.collector.current_label()}')
            if self.on_elapsed_cb:
                self.on_elapsed_cb()
        return elapsed

    def is_iteration_collected(self):
        return (self.collector.current_data_len() >= self.window)

    def flush(self):
        self.collector.flush()
        if self.collector.is_done():
            log.important(f'got all calibration data, proceeding')
            self.calibrate = DppCalibrate(
                self.config, collected_raw2model(self.collector.data))
        else:
            self.elapsed_triggered = False
            self.delay_next_milestone = self.delay
            self.last_milestone = time.monotonic()
            log.important(
                f'got calibration data for last label, now we wait {self.delay_next_milestone} ms before treating data for calibration {self.collector.current_label()}')

    def is_done(self):
        return self.collector.is_done()

    def transform(self, data):
        if self.calibrate:
            return self.calibrate.transform(data)
        elif self.is_elapsed():
            self.collector.collect(model2raw(data))
            if self.is_iteration_collected():
                self.flush()
            return data
        else:
            return data

import dpp_logging

from dpp_input import DppInput
from dpp_graph_setup import graph_setup
from dpp_graph import DppGraph
from dpp_graph4 import DppGraph4
from dpp_beep import beep
import dpp_config

dpp_logging.devLogging()
log = dpp_logging.getLogger(__name__)
config = dpp_config.setup()

beep()
graph_setup()

with DppInput(port=config['port']) as inp:
    index_of_sensor_to_fetch = 1
    graph = DppGraph4(config, title=f"Sensor index {index_of_sensor_to_fetch}")
    while True:
        res = inp.read()
        sensor = res[index_of_sensor_to_fetch]
        graph.append(sensor)

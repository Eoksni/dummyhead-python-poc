import os
from dpp_calibrate_manager import DppCalibrateManager
import dpp_logging

from dpp_input import DppInput
from dpp_graph_setup import graph_setup
from dpp_graph4 import DppGraph4
from dpp_beep import beep
import dpp_config
import numpy as np
from dpp_utils import apply_quaternion_to_vector

dpp_logging.devLogging()
log = dpp_logging.getLogger(__name__)
config = dpp_config.setup()

beep()
graph_setup()

with DppInput(port=config['port']) as inp:
    index_of_sensor_to_fetch = 1
    graph = DppGraph4(
        config, title=f"Sensor index {index_of_sensor_to_fetch} x-direction")
    calibrator = DppCalibrateManager(
        config, os.path.join(__file__, '../..', 'temp'))
    calibrator.initiate()
    while True:
        res = inp.read()
        calibrated = calibrator.transform(res)
        q = calibrated[index_of_sensor_to_fetch]
        px = np.array([0, 1, 0, 0])
        pxprime = apply_quaternion_to_vector(q, px)
        graph.append(pxprime)

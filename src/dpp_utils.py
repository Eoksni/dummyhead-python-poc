from typing import Union
import numpy as np
import quaternion

# our model here is just a list of 4-tuples


def raw2model(data: bytes):
    prepared = data.replace(b'\r\n', b'')
    splitted = prepared.split(b',')
    parsed = map(float, splitted)
    i = iter(parsed)
    return list(zip(i, i, i, i))


def model2raw(data):
    return b",".join([b",".join(map(lambda x: bytes(str(x), 'utf-8'), datum)) for datum in data])


def apply_quaternion_to_vector(q, p):
    # TODO: just work with quaternions by default
    qprime = quaternion.as_quat_array(q)
    pprime = quaternion.as_quat_array(p)
    qconj = np.conjugate(qprime)
    return quaternion.as_float_array(qprime * pprime * qconj)


def run_once(f):
    def wrapper(*args, **kwargs):
        if not wrapper.has_run:
            wrapper.has_run = True
            return f(*args, **kwargs)
    wrapper.has_run = False
    return wrapper

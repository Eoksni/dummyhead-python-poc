import matplotlib
import matplotlib.pyplot as plt


def graph_setup():
    matplotlib.use("tkAgg")

    plt.ion()

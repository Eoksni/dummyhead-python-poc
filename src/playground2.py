import serial

with serial.Serial('COM6', baudrate=115200, timeout=15) as ser:
    print(ser.name)         # check which port was really used
    s = ser.read(256)
    print('received')
    print(len(s))
    print(s)
    print(s.decode('ascii'))
    hexed = s.hex()
    print(len(hexed))
    print(hexed)

import numpy as np
import matplotlib.pyplot as plt
import math
from dpp_logging import getLogger
import throttle

log = getLogger(__name__)


def getLine(plot_result):
    return plot_result[0]


def shift(arr, num, fill_value=np.nan):
    result = np.empty_like(arr)
    if num > 0:
        result[:num] = fill_value
        result[num:] = arr[:-num]
    elif num < 0:
        result[num:] = fill_value
        result[:num] = arr[-num:]
    else:
        result[:] = arr
    return result


class DppGraphAllAngles:
    def __init__(self, config):
        self.plot_window = config['plot_window']
        self.number_of_variables = 6
        self.data = np.zeros(
            (self.number_of_variables, self.plot_window))
        ncols = 2
        nrows = math.ceil(self.number_of_variables / ncols)
        self.fig, axs = plt.subplots(
            nrows=nrows, ncols=ncols, sharey=True, sharex=True
        )
        self.axs = axs.flatten()
        for i in range(self.number_of_variables):
            ax = self.axs[i]
            ax.set_ylim(bottom=0, top=180)
            ax.set_xlim(left=0, right=self.plot_window-1)

        self.lines = [
            getLine(self.axs[index_var].plot(self.data[index_var]))
            for index_var in range(self.number_of_variables)
        ]

        for ax in self.axs:
            ax.autoscale_view()

        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

    @throttle.wrap(0.2, 1)
    def redraw(self):
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

    def set_titles(self, titles):
        for index_var in range(self.number_of_variables):
            self.axs[index_var].set_title(titles[index_var])

    def append(self, data):
        for index_var in range(self.number_of_variables):
            self.data[index_var] = shift(self.data[index_var], -1)
            self.data[index_var][self.plot_window - 1] = data[index_var]
            self.lines[index_var].set_ydata(self.data[index_var])

        self.redraw()

import dpp_logging

from dpp_input import DppInput
from dpp_graph_setup import graph_setup
from dpp_graph_all import DppGraphAll
from dpp_beep import beep
import dpp_config

dpp_logging.devLogging()
log = dpp_logging.getLogger(__name__)
config = dpp_config.setup()

beep()
graph_setup()

with DppInput(port=config['port']) as inp:
    graph = DppGraphAll(config)

    while True:
        res = inp.read()
        graph.append(res)

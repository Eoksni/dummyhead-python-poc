import os
import tensorflow as tf
from dpp_data import data_preprocess, get_dataset_series_one_file_path, get_single_prediction, read_dataset_label_one
from dpp_direction import DppDirection
from dpp_angles import DppAngles
import dpp_config
import json

import dpp_logging
import dpp_config

dpp_logging.devLogging()
log = dpp_logging.getLogger(__name__)
config = dpp_config.setup()
model_dir = os.path.join(__file__, "../..", config['data_model_dir'])

model = tf.keras.models.load_model(os.path.join(model_dir, "model.h5"))
with open(os.path.join(model_dir, "model_meta.json"), 'r') as f:
    metadata = json.load(f)

# TODO: may be it makes sense to extract defining dependencies to some common place so we
# can be sure it is all initiated in same way across different modules
direction = DppDirection(config)
angles = DppAngles(config)

label = "raise-right-hands"
series = "2021-06-01 15-58-08"
name = "2021-06-01 16-00-24 630"
df = read_dataset_label_one(
    config, direction, angles, label, get_dataset_series_one_file_path(config, label, series, name))

test_data = data_preprocess(df['data'])

log.info(f'Predict with chosen test data ({label} / {name}):')
prediction_label, prediction_confidence, prediction_index = get_single_prediction(
    model, metadata, test_data[0])

log.info(
    f'prediction: "{prediction_label}" with confidence {"{0:.0%}".format(prediction_confidence)}')

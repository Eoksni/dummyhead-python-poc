import os

import throttle
from dpp_angles import DppAngles
from dpp_calibrate_manager import DppCalibrateManager
from dpp_direction import DppDirection
from dpp_graph import DppGraph
from dpp_graph3 import DppGraph3
import dpp_logging
from dpp_utils import model2raw, run_once

from dpp_input import DppInput
from dpp_graph_setup import graph_setup
from dpp_graph4 import DppGraph4
from dpp_beep import beep
import dpp_config

dpp_logging.devLogging()
log = dpp_logging.getLogger(__name__)
config = dpp_config.setup()

beep()
graph_setup()


@throttle.wrap(5, 1)
def show_values(data):
    log.debug(f'data {data}')


with DppInput(port=config['port']) as inp:
    index_of_sensor_to_fetch_s = 1
    index_of_sensor_to_fetch_f = 3
    # graph1 = DppGraph4(
    #     config, title=f"Sensor index {index_of_sensor_to_fetch}")
    # graph2 = DppGraph4(
    #     config, title=f"Sensor index {index_of_sensor_to_fetch} calibrated")
    graph1 = DppGraph4(
        config, title=f"Sensor index {index_of_sensor_to_fetch_s} x-axis calibrated")
    graph2 = DppGraph4(
        config, title=f"Sensor index {index_of_sensor_to_fetch_f} x-axis calibrated")
    graph3 = DppGraph(
        config, title=f"Angle")

    calibrator = DppCalibrateManager(
        config, os.path.join(__file__, '../..', 'temp'))
    calibrator.initiate()
    dir = DppDirection(config)
    angle = DppAngles(config)

    while True:
        res = inp.read()
        calibrated = calibrator.transform(res)
        # dirs = calibrator.transform(res)
        dirs = dir.transform(calibrated)
        angles = angle.transform(dirs)
        data1 = dirs[index_of_sensor_to_fetch_s]
        data2 = dirs[index_of_sensor_to_fetch_f]
        data3 = angles['left_elbow']
        # data3 = dir.transform(res)[index_of_sensor_to_fetch]
        # data4 = dir.transform(calibrated)[index_of_sensor_to_fetch]
        graph1.append(data1)
        graph2.append(data2)
        graph3.append(data3)
        show_values({'data1': data1, 'data2': data2, 'data3': data3})

        if (calibrator.is_done()):
            pass

import logging

from dpp_beep import beep

NOISE = 5


class Logger:
    def __init__(self, logger: logging.Logger):
        self.logger = logger

    def noise(self, *kargs, **kwargs):
        return self.logger.log(NOISE, *kargs, **kwargs)

    def debug(self, *kargs, **kwargs):
        return self.logger.debug(*kargs, **kwargs)

    def log(self, *kargs, **kwargs):
        return self.logger.log(*kargs, **kwargs)

    def info(self, *kargs, **kwargs):
        return self.logger.info(*kargs, **kwargs)

    def important(self, *kargs, **kwargs):
        beep(level='lower')
        return self.logger.info(*kargs, **kwargs)

    def urgent(self, *kargs, **kwargs):
        beep(level='normal')
        return self.logger.info(*kargs, **kwargs)

    def error(self, *kargs, **kwargs):
        return self.logger.error(*kargs, **kwargs)


def devLogging():
    logging.basicConfig()


def getLogger(name):
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)

    return Logger(logger)

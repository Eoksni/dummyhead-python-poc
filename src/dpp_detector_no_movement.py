import numpy as np
from dpp_logging import getLogger
from dpp_beep import beep
import throttle

log = getLogger(__name__)
seconds_between_beeps = 5


@throttle.wrap(seconds_between_beeps, 1)
def try_no_movement_beep():
    beep(level='lower')


class DppDetectorNoMovement:
    def __init__(self, config, beep=True):
        self.threshold_in = config['detector_no_movement_threshold_in']
        self.threshold_out = config['detector_no_movement_threshold_out']
        self.window = config['detector_no_movement_window']
        self.data = []
        self.is_moving = False
        self.beep = beep

    def check(self, data):
        if (len(self.data) < self.window):
            self.data.append(data)
        elif (len(self.data) >= self.window):
            self.data = self.data[1:]
            self.data.append(data)
        if (len(self.data) < self.window):
            return
        d = np.array(self.data)
        averages = np.average(d, axis=0)
        diffs = np.square(d - averages)
        total = np.sqrt(diffs.sum())
        log.noise(f'total diff {total}')
        threshold = self.threshold_in if self.is_moving else self.threshold_out
        if (total < threshold):
            if (self.beep):
                try_no_movement_beep()
            self.is_moving = False
        else:
            self.is_moving = True
        return self.is_moving

import os
from dpp_calibrate_manager import DppCalibrateManager
import dpp_logging

from dpp_direction import DppDirection
from dpp_angles import DppAngles
from dpp_detector_no_movement import DppDetectorNoMovement
from dpp_input import DppInput
from dpp_graph_setup import graph_setup
from dpp_beep import beep
import dpp_config
from dpp_utils import run_once
from dpp_graph_all_angles import DppGraphAllAngles

dpp_logging.devLogging()
log = dpp_logging.getLogger(__name__)
config = dpp_config.setup()

beep()
graph_setup()

with DppInput(port=config['port']) as inp:
    graph = DppGraphAllAngles(config)
    calibrator = DppCalibrateManager(
        config, os.path.join(__file__, '../..', 'temp'))
    calibrator.initiate()
    direction = DppDirection(config)
    angles = DppAngles(config)
    detector_no_movement = DppDetectorNoMovement(config)

    @run_once
    def once_set_titles(titles):
        graph.set_titles(titles)

    while True:
        res = inp.read()
        calibrated = calibrator.transform(res)
        dirs = direction.transform(calibrated)
        angs = angles.transform(dirs)
        angs_values = list(angs.values())
        once_set_titles(list(angs.keys()))
        if (calibrator.is_done()):
            detector_no_movement.check(list(angs.values()))
        graph.append(angs_values)

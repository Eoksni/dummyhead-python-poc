import dpp_logging

from dpp_input import DppInput
from dpp_graph_setup import graph_setup
from dpp_graph import DppGraph
from dpp_graph3 import DppGraph3
from dpp_beep import beep
import dpp_config

dpp_logging.devLogging()
log = dpp_logging.getLogger(__name__)
config = dpp_config.setup()

beep()
graph_setup()

with DppInput(port=config['port']) as inp:
    graph = DppGraph3(config, title='Sensor 1')
    while True:
        beep()
        res = inp.read()
        sensor = res[1]  # 1-indexed sensor
        graph.append(sensor)

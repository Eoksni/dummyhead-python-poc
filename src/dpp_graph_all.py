import numpy as np
import matplotlib.pyplot as plt
import math
from dpp_logging import getLogger

log = getLogger(__name__)


def getLine(plot_result):
    return plot_result[0]


def shift(arr, num, fill_value=np.nan):
    result = np.empty_like(arr)
    if num > 0:
        result[:num] = fill_value
        result[num:] = arr[:-num]
    elif num < 0:
        result[num:] = fill_value
        result[:num] = arr[-num:]
    else:
        result[:] = arr
    return result


MAX_COUNTER = 10


class DppGraphAll:
    def __init__(self, config):
        self.counter = MAX_COUNTER - 1

        self.plot_window = config['plot_window']
        self.number_of_sensors = config['number_of_sensors']
        self.variables_to_plot = ['w', 'x', 'y', 'z']
        self.number_of_variables = len(self.variables_to_plot)
        self.data = np.zeros(
            (self.number_of_sensors, self.number_of_variables, self.plot_window))
        ncols = 2
        nrows = math.ceil(self.number_of_sensors / ncols)
        self.fig, axs = plt.subplots(
            nrows=nrows, ncols=ncols, sharey=True, sharex=True
        )
        self.axs = axs.flatten()
        for i in range(self.number_of_sensors):
            ax = self.axs[i]
            ax.set_ylim(bottom=-1.1, top=1.1)
            ax.set_title(f'Sensor index {i}')

        self.lines = [
            [
                getLine(self.axs[index_sensor].plot(
                    self.data[index_sensor][index_var],
                    label=self.variables_to_plot[index_var])
                )
                for index_var in range(self.number_of_variables)
            ]
            for index_sensor in range(self.number_of_sensors)
        ]
        self.legs = [ax.legend(loc='upper left', frameon=False)
                     for ax in self.axs]

    def append(self, data):
        for index_sensor in range(self.number_of_sensors):
            for index_var in range(self.number_of_variables):
                self.data[index_sensor][index_var] = shift(
                    self.data[index_sensor][index_var], -1)
                self.data[index_sensor][index_var][
                    self.plot_window - 1] = data[index_sensor][index_var]
                self.lines[index_sensor][index_var].set_ydata(
                    self.data[index_sensor][index_var])

        # performance hack
        self.counter += 1
        if (self.counter >= MAX_COUNTER):
            for ax in self.axs:
                ax.autoscale_view()

            self.fig.canvas.draw()
            self.fig.canvas.flush_events()

            self.counter = 0

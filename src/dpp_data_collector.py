import numpy as np
from datetime import datetime
import os
from dpp_logging import getLogger
import random

log = getLogger(__name__)


class DppDataCollectorBase:
    def __init__(self, window):
        self.data = []
        self.window = window

    def collect(self, data):
        self.data.append(data)

    def precollect(self, data):
        if (len(self.data) >= self.window):
            self.data = self.data[1:]
        self.data.append(data)

    def flush(self, path_file):
        with open(path_file, 'wb') as f:
            f.write(b'\n'.join(self.data) + b'\n')
            self.data = []


class DppDataCollectorInner:
    def __init__(self, label, path_dir, config):
        self.label = label
        self.path_dir = path_dir
        self.window = config['collect_prewindow']
        self.base = DppDataCollectorBase(window=self.window)

    def collect(self, data):
        self.base.collect(data)

    def precollect(self, data):
        self.base.precollect(data)

    def flush(self):
        # salt is some naive way of preventing filename collision
        # should be good enough, it probably is never going to collide anyway
        # in real usage scenarios
        salt = '{:03}'.format(random.randrange(1, 999))
        file_name = datetime.now().strftime(f"%Y-%m-%d %H-%M-%S {salt}.csv")
        path_file = os.path.join(self.path_dir, file_name)
        self.base.flush(path_file)


class DppDataCollector:
    def __init__(self, label, config):
        self.label = label
        self.data_output_dir = config['data_output_dir']
        self.config = config
        pass

    def __enter__(self):
        series_name = datetime.now().strftime(f"%Y-%m-%d %H-%M-%S")
        path_dir = os.path.join(
            __file__, "../..", self.data_output_dir, self.label, series_name)
        log.debug(f"path_dir: {path_dir}")
        os.makedirs(path_dir, exist_ok=True)
        inner = DppDataCollectorInner(
            label=self.label, path_dir=path_dir, config=self.config)
        return inner

    def __exit__(self, *kargs, **kwargs):
        pass


CALIBRATE_ORDER = ['front',
                   'back',
                   'front_arms',
                   'front_left_hip',
                   'front_right_hip'
                   ]


class DppCalibrateCollector:
    def __init__(self, config, path_dir):
        self.path_dir = path_dir
        self.window = config['collect_prewindow']
        self.base = DppDataCollectorBase(window=self.window)
        self.order = 0
        self.data = {
            'front': [],
            'front_arms': [],
            'front_left_hip': [],
            'front_right_hip': [],
            'back': []
        }

    def collect(self, data):
        self.base.collect(data)

    def precollect(self, data):
        self.base.precollect(data)

    def flush(self):
        label = self.current_label()
        path_file = os.path.join(
            self.path_dir, f"calibration_{label}.csv")
        self.data[label] = np.array(self.base.data)
        self.base.flush(path_file)
        self.order += 1

    def current_data_len(self):
        return len(self.base.data)

    def current_label(self):
        return CALIBRATE_ORDER[self.order]

    def is_done(self):
        return (self.order >= len(CALIBRATE_ORDER))

import winsound

frequency = {
    'normal': 500,
    'lower': 200
}
duration = 250

beeped = dict()


def beep(idn=None, level='normal'):
    if (not (idn is None)):
        if idn in beeped:
            return
        else:
            beeped[idn] = True
    winsound.Beep(frequency[level], duration)

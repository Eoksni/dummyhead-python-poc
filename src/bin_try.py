import os
import tensorflow as tf
from dpp_calibrate_manager import DppCalibrateManager
from dpp_data import data_preprocess, get_dataset_dir, get_single_prediction, read_calibration
from dpp_direction import DppDirection
from dpp_angles import DppAngles
from dpp_detector_no_movement import DppDetectorNoMovement
from dpp_interval import DppInterval
from dpp_input import DppInput
from dpp_graph_setup import graph_setup
from dpp_graph_all_angles import DppGraphAllAngles
from dpp_beep import beep
import dpp_config
import json

import dpp_transform
import dpp_logging
import dpp_config
from dpp_utils import run_once

dpp_logging.devLogging()
log = dpp_logging.getLogger(__name__)
config = dpp_config.setup()
model_dir = os.path.join(__file__, "../..", config['data_model_dir'])

model = tf.keras.models.load_model(os.path.join(model_dir, "model.h5"))
with open(os.path.join(model_dir, "model_meta.json"), 'r') as f:
    metadata = json.load(f)

beep()
graph_setup()

with DppInput(port=config['port']) as inp:
    graph = DppGraphAllAngles(config)
    direction = DppDirection(config)
    angles = DppAngles(config)
    interval = DppInterval()
    detector_no_movement = DppDetectorNoMovement(config, beep=False)
    data = []
    raw_data = []
    calibrator = DppCalibrateManager(
        config, os.path.join(__file__, '../..', 'temp'))
    calibrator.initiate()

    # calibrator = read_calibration(config, os.path.join(
    #     get_dataset_dir(config), 'sit-stand-without-hands/2021-06-01 16-39-42'))

    @run_once
    def once_set_titles(titles):
        graph.set_titles(titles)

    while True:
        res = inp.read()
        # TODO: damn, this transform is stateful which led to a bug already
        # try to rethink how it should work to avoid that
        calibrated = calibrator.transform(res)
        dirs = direction.transform(calibrated)
        angs = angles.transform(dirs)
        angs_values = list(angs.values())
        once_set_titles(list(angs.keys()))

        if (calibrator.is_done()):
            # transformed_data = dpp_transform.transform(
            # calibrator, direction, angles, res)
            transformed_data = dpp_transform.transform(
                calibrator.calibrate, direction, angles, res)

            is_moving = detector_no_movement.check(angs_values)
            interval.check(is_moving)
            if (interval.just_stopped()):
                beep(level='lower')

                datas = data_preprocess([data])
                processed = datas[0]

                prediction_label, prediction_confidence, prediction_index = get_single_prediction(
                    model, metadata, processed)

                log.info(
                    f'prediction: "{prediction_label}" with confidence {"{0:.0%}".format(prediction_confidence)}')

                data = []
                raw_data = []
            elif (interval.is_moving()):
                data.append(transformed_data)
                raw_data.append(res)
            else:
                if (len(data) >= config['collect_prewindow']):
                    data = data[1:]
                data.append(transformed_data)
                raw_data.append(res)

        graph.append(angs_values)

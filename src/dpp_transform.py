import numpy as np
from dpp_angles import DppAngles
from dpp_calibrate import DppCalibrate
from dpp_direction import DppDirection
from dpp_logging import getLogger

log = getLogger(__name__)


def transform_3d_vectors(calibrator: DppCalibrate, direction: DppDirection, angles: DppAngles, data):
    calibrated = calibrator.transform(data)
    dirs = direction.transform(calibrated)
    vectors = dirs[:, 1:]
    flat = vectors.flatten()
    return flat


def transform_angles(calibrator: DppCalibrate, direction: DppDirection, angles: DppAngles, data):
    calibrated = calibrator.transform(data)
    dirs = direction.transform(calibrated)
    angs = angles.transform(dirs)
    angs_values = np.array(list(angs.values()))
    return angs_values


transform = transform_angles

from dpp_calibrate_manager import DppCalibrateManager
import dpp_logging
from dpp_direction import DppDirection
from dpp_angles import DppAngles
from dpp_detector_no_movement import DppDetectorNoMovement
from dpp_data_collector import DppDataCollector
from dpp_interval import DppInterval
from dpp_input import DppInput
from dpp_graph_setup import graph_setup
from dpp_graph_all_angles import DppGraphAllAngles
from dpp_beep import beep
import dpp_config
from dpp_utils import model2raw, run_once

dpp_logging.devLogging()
log = dpp_logging.getLogger(__name__)
config = dpp_config.setup()

beep()
graph_setup()

with DppInput(port=config['port']) as inp:
    label = config["collect_label"]

    graph = DppGraphAllAngles(config)
    direction = DppDirection(config)
    angles = DppAngles(config)
    interval = DppInterval()
    detector_no_movement = DppDetectorNoMovement(config, beep=False)

    with DppDataCollector(label=label, config=config) as collector:
        calibrator = DppCalibrateManager(config, collector.path_dir)
        calibrator.initiate()

        @run_once
        def once_set_titles(titles):
            graph.set_titles(titles)

        while True:
            res = inp.read()

            calibrated = calibrator.transform(res)
            dirs = direction.transform(calibrated)
            angs = angles.transform(dirs)
            angs_values = list(angs.values())
            once_set_titles(list(angs.keys()))
            if (calibrator.is_done()):
                is_moving = detector_no_movement.check(angs_values)
                interval.check(is_moving)
                if (interval.just_stopped()):
                    beep(level='lower')
                    collector.flush()
                elif (interval.is_moving()):
                    collector.collect(model2raw(res))
                else:
                    collector.precollect(model2raw(res))
            graph.append(angs_values)

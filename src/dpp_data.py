import os
import pandas as pd
import numpy as np
import glob
from dpp_angles import DppAngles
from dpp_calibrate import DppCalibrate
from dpp_data_collector import CALIBRATE_ORDER
from dpp_direction import DppDirection
import dpp_transform
import tensorflow as tf
import dpp_logging
from dpp_utils import raw2model
from sklearn import preprocessing
from scipy.interpolate import interp1d

log = dpp_logging.getLogger(__name__)


def get_dataset_dir(config):
    return os.path.join(__file__, "../..", config['data_output_dir'])


def get_model_dir(config):
    return os.path.join(__file__, "../..", config['data_model_dir'])


def get_single_prediction(model, metadata, data):
    r_data = tf.ragged.constant([data])
    predictions = model.predict(r_data)
    log.debug(f'predictions: {predictions}')
    prediction_index = predictions.argmax(axis=1)[0]
    prediction_label = metadata['columns'][prediction_index]
    prediction_confidence = predictions[0][prediction_index]
    return (prediction_label, prediction_confidence, prediction_index)


def get_dataset_series_one_file_path(config, label, series, name):
    return os.path.join(get_dataset_dir(config), label, series, f"{name}.csv")


def parse_dataset_dir_name(dir_name):
    return dir_name


def read_dataset_file_raw(file_path):
    with open(file_path, 'rb') as f:
        return f.readlines()


def read_dataset_file(calibrator: DppCalibrate, direction: DppDirection, angles: DppAngles, file_path):
    raw_data = read_dataset_file_raw(file_path)
    data = np.array([raw2model(raw_datum) for raw_datum in raw_data])
    # TODO: can be speed up by vectorizing all this stuff
    transformed_data = []
    for datum in data:
        transformed_data.append(dpp_transform.transform(
            calibrator, direction, angles, datum))
    return np.array(transformed_data)


def read_calibration_file(file_path):
    raw_data = read_dataset_file_raw(file_path)
    data = np.array([raw2model(raw_datum) for raw_datum in raw_data])
    return data


def read_calibration(config, dir_path):
    calibrations = CALIBRATE_ORDER
    data = {c: read_calibration_file(os.path.join(
        dir_path, f'calibration_{c}.csv')) for c in calibrations}
    calibrator = DppCalibrate(config, data)
    return calibrator


def read_dataset_label_series(config, direction, angles, label, dir_path):
    calibrator = read_calibration(config, dir_path)

    all_files = glob.glob(os.path.join(dir_path, "*.csv"))
    li = [read_dataset_file(
        calibrator,
        direction,
        angles,
        filepath
        # hack to ignore calibration file
    ) for filepath in all_files if os.path.basename(filepath) != "calibration.csv"]

    frame = pd.DataFrame()
    frame['data'] = li
    frame['label'] = label

    return frame


def read_dataset_label_one(config, direction, angles, label, file_path):
    dir_path = os.path.dirname(file_path)
    calibrator = read_calibration(config, dir_path)

    li = [read_dataset_file(calibrator, direction, angles, file_path)]

    frame = pd.DataFrame()
    frame['data'] = li
    frame['label'] = label

    return frame


def read_dataset_label(config, direction, angles, dir_path):
    label = parse_dataset_dir_name(os.path.basename(dir_path))
    if (not label):
        raise NotImplementedError()

    all_series = glob.glob(os.path.join(dir_path, "*"))
    dfs = [read_dataset_label_series(config, direction, angles, label, series_dir)
           for series_dir in all_series]

    frame = pd.concat(dfs, axis=0, ignore_index=True)

    return frame


def read_datasets(config, direction, angles, dir_path):
    all_dirs = glob.glob(os.path.join(dir_path, "*"))
    frames = [read_dataset_label(config, direction, angles, dir_path)
              for dir_path in all_dirs]
    return pd.concat(frames, axis=0, ignore_index=True)


PREPROCESS_WINDOW = 100
PREPROCESS_CROP_LOOSE = 5


def is_constant(xs):
    averages = np.average(xs)
    diffs = np.square(xs - averages)
    total = np.sqrt(diffs.sum())
    threshold = 15
    return total < threshold


def angle_value_preprocess(angles):
    if is_constant(angles):
        return ('is_constant', angles - np.min(angles))
    else:
        return ('is_constant', angles - np.min(angles))
        # return ('is_not_constant', preprocessing.minmax_scale(angles, feature_range=(0, 180)))


def angle_crop_preprocess_left(angles):
    # TODO: optimize
    for i in range(len(angles)):
        if not is_constant(angles[:i]):
            index_to_crop = max(0, i - PREPROCESS_CROP_LOOSE)
            return angles[index_to_crop:]
    return angles


def angle_crop_preprocess(angles):
    left_cropped = angle_crop_preprocess_left(angles)
    both_cropped_reversed = angle_crop_preprocess_left(np.flip(left_cropped))
    return np.flip(both_cropped_reversed)


def angle_window_preprocess(angles):
    xs = np.linspace(0, 1, num=len(angles), endpoint=True)
    f = interp1d(xs, angles)
    xsnew = np.linspace(0, 1, num=PREPROCESS_WINDOW, endpoint=True)
    return f(xsnew)


def angle_data_preprocess(angles):
    constanted, value_processed = angle_value_preprocess(angles)
    def maybe_crop(x): return x if constanted else angle_crop_preprocess
    cropped = maybe_crop(value_processed)
    window_processed = angle_window_preprocess(cropped)
    return window_processed


def single_series_preprocess(series):
    return np.transpose(
        np.array([
            angle_data_preprocess(angle_data)
            for angle_data in np.transpose(series)
        ])
    )


def data_preprocess(data):
    return np.array([single_series_preprocess(series) for series in data])

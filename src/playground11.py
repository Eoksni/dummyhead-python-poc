import os
from dpp_calibrate_manager import DppCalibrateManager
import dpp_logging
import numpy as np
import matplotlib.pyplot as plt

from dpp_direction import DppDirection
from dpp_angles import DppAngles
from dpp_detector_no_movement import DppDetectorNoMovement
from dpp_input import DppInput
from dpp_graph_setup import graph_setup
from dpp_beep import beep
import dpp_config
import numpy as np
from dpp_utils import run_once

dpp_logging.devLogging()
log = dpp_logging.getLogger(__name__)
config = dpp_config.setup()

beep()
graph_setup()


class DppGraph:
    def __init__(self, config, title):
        self.plot_window = config['plot_window']
        self.y_var = np.array(np.zeros([self.plot_window]))
        self.y_var = np.array(np.zeros([self.plot_window]))
        self.fig, self.ax = plt.subplots()
        self.ax.set_title(title)
        self.ax.set_ylim(bottom=-1, top=1)
        self.line, = self.ax.plot(self.y_var)

    def append(self, data):
        self.y_var = np.append(self.y_var, data)
        self.y_var = self.y_var[1:self.plot_window+1]
        self.line.set_ydata(self.y_var)
        # self.ax.relim()
        self.ax.autoscale_view()
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()


with DppInput(port=config['port']) as inp:
    graph = DppGraph(config)
    calibrator = DppCalibrateManager(
        config, os.path.join(__file__, '../..', 'temp'))
    calibrator.initiate()
    direction = DppDirection(config)
    angles = DppAngles(config)
    detector_no_movement = DppDetectorNoMovement(config)

    @run_once
    def once_set_titles(titles):
        graph.set_titles(titles)

    while True:
        res = inp.read()
        calibrated = calibrator.transform(res)
        if (calibrated is None):
            pass
        else:
            dirs = direction.transform(calibrated)
            angs = angles.transform(dirs)
            angs_values = list(angs.values())
            once_set_titles(list(angs.keys()))
            if (calibrator.is_done()):
                detector_no_movement.check(list(angs.values()))
            graph.append(angs_values)

import os
from dpp_calibrate_manager import DppCalibrateManager
import dpp_logging

from dpp_input import DppInput
from dpp_graph_setup import graph_setup
from dpp_graph_all import DppGraphAll
from dpp_beep import beep
import dpp_config

dpp_logging.devLogging()
log = dpp_logging.getLogger(__name__)
config = dpp_config.setup()

beep()
graph_setup()

with DppInput(port=config['port']) as inp:
    graph = DppGraphAll(config)
    calibrator = DppCalibrateManager(
        config, os.path.join(__file__, '../..', 'temp'))
    calibrator.initiate()
    while True:
        res = inp.read()
        calibrated = calibrator.transform(res)
        graph.append(calibrated)

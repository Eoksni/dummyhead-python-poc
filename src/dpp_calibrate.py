from shutil import ExecError
import numpy as np
from average_quaternions import averageQuaternions
from dpp_direction import DppDirection
from dpp_logging import getLogger
import quaternion
from numpy import linalg as LA

from dpp_utils import apply_quaternion_to_vector

log = getLogger(__name__)


def get_z(q_front, q_back):
    # return [0., 0., 1.]
    q_front_prime = np.conjugate(q_front)
    p = quaternion.as_float_array(q_back * q_front_prime)
    v = p[1:]
    abs = LA.norm(v)
    z = v / abs
    return z


def get_x(z, q_front, q_front_raised):
    # return [0., 1., 0.]
    q_front_prime = np.conjugate(q_front)
    p = quaternion.as_float_array(q_front_raised * q_front_prime)
    v = p[1:]
    v_projection = z * np.dot(v, z) / np.dot(z, z)
    x_candidate = v - v_projection
    abs = LA.norm(x_candidate)
    x = x_candidate / abs
    return x


def get_y(x, z):
    # return [-1., 0., 0.]
    v = np.cross(z, x)
    abs = LA.norm(v)
    y = v / abs
    return y


def get_a(x, y, z):
    a = np.array(
        [
            [x[0], y[0], z[0]],
            [x[1], y[1], z[1]],
            [x[2], y[2], z[2]]
        ])
    return a


def get_t(a):
    # convert rotation matrix to quaternion in new coords, which rotates new basis into old basis position
    w = np.sqrt(max(0, 1 + a[0][0] +
                a[1][1] + a[2][2])) / 2
    x = np.sqrt(max(0, (a[0][0] + 1 - 2 * w * w)) / 2)
    y = np.sqrt(max(0, (a[1][1] + 1 - 2 * w * w)) / 2)
    z = np.sqrt(max(0, (a[2][2] + 1 - 2 * w * w)) / 2)
    t_candidate = quaternion.as_quat_array([w, x, y, z])
    abs = np.norm(t_candidate)
    t = quaternion.as_float_array(t_candidate) / abs
    return t


def convert_rotation_into_new_coords(a_reverted, p0):
    w0 = p0[0]
    v0 = p0[1:]

    w1 = w0
    v1 = np.dot(a_reverted, v0)

    # q_candidate = quaternion.as_quat_array([w1, v1[0], v1[1], v1[2]])
    # abs = np.norm(q_candidate)
    # q = q_candidate / abs

    return np.array([w1, v1[0], v1[1], v1[2]])


class DppCalibrate:
    def __init__(self, config, data):
        self.number_of_sensors = config['number_of_sensors']
        self.window = config['calibrate_window']
        self.number_of_variables = 4
        self.right_shoulder = config['sensor_right_shoulder']
        self.left_shoulder = config['sensor_left_shoulder']
        self.right_forearm = config['sensor_right_forearm']
        self.left_forearm = config['sensor_left_forearm']
        self.right_hip = config['sensor_right_hip']
        self.left_hip = config['sensor_left_hip']

        self._initialize(data)

    def get_average(self, data):
        qs = np.zeros((self.number_of_sensors, self.window,
                       self.number_of_variables))
        for i in range(self.window):
            for j in range(self.number_of_sensors):
                for k in range(self.number_of_variables):
                    qs[j][i][k] = data[i][j][k]

        return quaternion.as_quat_array([averageQuaternions(q) for q in qs])

    def _initialize(self, data):
        log.info(f'calibration initialized')

        averages = {
            'front': self.get_average(data['front']),
            'front_arms': self.get_average(data['front_arms']),
            'front_left_hip': self.get_average(data['front_left_hip']),
            'front_right_hip': self.get_average(data['front_right_hip']),
            'back': self.get_average(data['back']),
        }

        zs = np.array([get_z(f, b)
                      for (f, b) in zip(averages['front'], averages['back'])])

        log.debug('zs')
        log.debug(zs)

        xs = [None]*self.number_of_sensors
        xs[self.right_shoulder] = get_x(zs[self.right_shoulder],
                                        averages['front'][self.right_shoulder],
                                        averages['front_arms'][self.right_shoulder]
                                        )
        xs[self.left_shoulder] = get_x(zs[self.left_shoulder],
                                       averages['front'][self.left_shoulder],
                                       averages['front_arms'][self.left_shoulder]
                                       )
        xs[self.right_forearm] = get_x(zs[self.right_forearm],
                                       averages['front'][self.right_forearm],
                                       averages['front_arms'][self.right_forearm]
                                       )
        xs[self.left_forearm] = get_x(zs[self.left_forearm],
                                      averages['front'][self.left_forearm],
                                      averages['front_arms'][self.left_forearm]
                                      )
        xs[self.right_hip] = get_x(zs[self.right_hip],
                                   averages['front'][self.right_hip],
                                   averages['front_right_hip'][self.right_hip]
                                   )
        xs[self.left_hip] = get_x(zs[self.left_hip],
                                  averages['front'][self.left_hip],
                                  averages['front_left_hip'][self.left_hip]
                                  )

        xs = np.array(xs)

        ys = np.array([get_y(x, z) for (x, z) in zip(xs, zs)])

        as_normal = np.array([get_a(x, y, z) for (x, y, z) in zip(xs, ys, zs)])

        log.debug(as_normal)

        self.as_reverted = np.linalg.inv(as_normal)

        self.ts = np.array([get_t(a) for a in as_normal])

        log.debug(f'averaged calibration matrices: {self.as_reverted}')
        log.debug(f'averaged calibration quaternions: {self.ts}')

    def transform(self, data):
        # return data
        # px = np.repeat([[0, 1, 0, 0]], self.number_of_sensors, axis=0)
        # pxprime = apply_quaternion_to_vector(data, px)

        # calibrated_dirs = np.array([apply_matrix_to_quaternion_vector(
        #     a_reverted, p) for (a_reverted, p) in zip(self.as_reverted, pxprime)])

        # return calibrated_dirs

        # TODO: remove try
        try:
            p1 = np.array([convert_rotation_into_new_coords(a_reverted, p0)
                           for (a_reverted, p0) in zip(self.as_reverted, data)])
            return quaternion.as_float_array(quaternion.as_quat_array(p1) * quaternion.as_quat_array(self.ts))
        except:
            log.error('shouldn\'t happen!')
            return data


def apply_matrix_to_quaternion_vector(m, p):
    v = p[1:]
    res = np.dot(m, v)
    return [0, res[0], res[1], res[2]]

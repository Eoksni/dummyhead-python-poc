import numpy as np
import matplotlib.pyplot as plt
import attr
import time
import matplotlib
import throttle


class DppGraph:
    def __init__(self, config, title):
        self.plot_window = config['plot_window']
        self.y_var = np.array(np.zeros([self.plot_window]))
        self.fig, self.ax = plt.subplots()
        self.ax.set_title(title)
        self.ax.set_ylim(bottom=0, top=180)
        self.line, = self.ax.plot(self.y_var)

        @throttle.wrap(0.2, 1)
        def redraw(self):
            self._redraw()

        self.redraw = redraw

    def _redraw(self):
        self.line.set_ydata(self.y_var)
        # self.ax.relim()
        self.ax.autoscale_view()
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

    def append(self, data):
        self.y_var = np.append(self.y_var, data)
        self.y_var = self.y_var[1:self.plot_window+1]
        self.redraw(self)

from math import ceil
import os
import numpy as np
import pandas as pd
import tensorflow as tf
import json
from dpp_angles import DppAngles
from dpp_direction import DppDirection

import dpp_logging
import dpp_config
from dpp_data import data_preprocess, read_datasets

dpp_logging.devLogging()
log = dpp_logging.getLogger(__name__)
config = dpp_config.setup()
dataset_dir = os.path.join(__file__, "../..", config['data_output_dir'])
model_dir = os.path.join(__file__, "../..", config['data_model_dir'])

direction = DppDirection(config)
angles = DppAngles(config)


epochs = 12

df = read_datasets(config, direction, angles, dataset_dir)
data = data_preprocess(df['data'])
num_batch, num_window, num_features = data.shape
onedf = pd.get_dummies(df['label'])

log.debug(f'num_features {num_features}')
log.debug(f"df.shape: {df.shape}")
log.debug(f"onedf.shape: {onedf.shape}")

train_index = pd.Index(np.random.choice(df.index, ceil(len(df) * 0.7)))
train_onedf = onedf.take(train_index)
train_data = data[train_index]
train_labels = train_onedf.to_numpy()

log.debug(f'train_onedf.shape: {train_onedf.shape}')
log.debug(f'train_labels.shape: {train_labels.shape}')
log.debug(f'train_data.shape: {train_data.shape}')

other_df = df.drop(train_index)

validation_index = pd.Index(np.random.choice(
    other_df.index, ceil(len(other_df) * 0.5)))
validation_onedf = onedf.take(validation_index)
validation_data = data[validation_index]
validation_labels = validation_onedf.to_numpy()

log.debug(f'validation_onedf.shape: {validation_onedf.shape}')
log.debug(f'validation_labels.shape: {validation_labels.shape}')
log.debug(f'validation_data.shape: {validation_data.shape}')

test_df = other_df.drop(validation_index)
test_onedf = onedf.take(test_df.index)
test_data = data[test_df.index]
test_labels = test_onedf.to_numpy()

log.debug(f'test_df.shape: {test_df.shape}')
log.debug(f'test_onedf.shape: {test_onedf.shape}')
log.debug(f'test_labels.shape: {test_labels.shape}')
log.debug(f'test_data.shape: {test_data.shape}')

metadata = {
    'columns': train_onedf.columns.tolist()
}
with open(os.path.join(model_dir, "model_meta.json"), 'w') as f:
    json.dump(metadata, f, indent=4)

model = tf.keras.Sequential()
model.add(tf.keras.layers.InputLayer(input_shape=(num_window, num_features)))
model.add(tf.keras.layers.LSTM(64))
model.add(tf.keras.layers.Dense(256))
model.add(tf.keras.layers.Dense(128))
model.add(tf.keras.layers.Dense(train_labels.shape[1], activation='softmax'))
model.compile(optimizer='adam',
              loss='categorical_crossentropy',
              metrics=['accuracy'])

model.fit(train_data, train_labels, epochs=epochs,
          validation_data=(validation_data, validation_labels))

log.info('Evaluate against test data:')
model.evaluate(test_data, test_labels)

model.save(os.path.join(model_dir, "model.h5"))

from shutil import ExecError
from dpp_logging import getLogger
import serial
import attr
import dpp_utils

log = getLogger(__name__)


@attr.s
class DppInput:
    port: str = attr.ib()
    _serial: serial.Serial = attr.ib(default=attr.Factory(
        lambda self: serial.Serial(self.port, baudrate=115200, timeout=15),
        takes_self=True)
    )

    def read(self):
        res = self._serial.readline()
        log.noise(f'read data {res}')
        parsed = dpp_utils.raw2model(res)
        log.noise(f'parsed data {parsed}')
        return parsed

    def __enter__(self):
        log.info('initializing')
        self._serial.__enter__()
        log.debug(
            'wait for next line to start fetching data starting with first sensor')
        _data = self._serial.readline()
        if (len(_data) == 0):
            raise ExecError()
        log.debug(f'got: {_data}')
        log.info('ready for data fetching!')
        return self

    def __exit__(self, *args, **kwargs):
        self._serial.__exit__(*args, **kwargs)

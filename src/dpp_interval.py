class DppInterval:
    def __init__(self):
        self.last_value = False
        self.current_value = False

    def just_flipped(self):
        return self.last_value != self.current_value

    def just_stopped(self):
        return (not self.is_moving()) and self.just_flipped()

    def is_moving(self):
        return self.current_value

    def check(self, value):
        self.last_value = self.current_value
        self.current_value = bool(value)

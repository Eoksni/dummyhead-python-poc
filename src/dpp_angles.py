import numpy as np
from dpp_logging import getLogger
import numpy.linalg as LA

log = getLogger(__name__)


def get_angle(a, b):
    inner = np.inner(a, b)
    norms = LA.norm(a) * LA.norm(b)

    cos = inner / norms
    rad = np.arccos(np.clip(cos, -1.0, 1.0))
    deg = np.rad2deg(rad)
    return deg


class DppAngles:
    def __init__(self, config):
        self.number_of_sensors = config['number_of_sensors']
        self.number_of_variables = 4
        self.right_shoulder = config['sensor_right_shoulder']
        self.left_shoulder = config['sensor_left_shoulder']
        self.right_forearm = config['sensor_right_forearm']
        self.left_forearm = config['sensor_left_forearm']
        self.right_hip = config['sensor_right_hip']
        self.left_hip = config['sensor_left_hip']

    # That pattern probably should be extracted as an interface/base class of some sort.
    def transform(self, data):
        right_shoulder = data[self.right_shoulder][1:]
        left_shoulder = data[self.left_shoulder][1:]
        right_forearm = data[self.right_forearm][1:]
        left_forearm = data[self.left_forearm][1:]
        right_hip = data[self.right_hip][1:]
        left_hip = data[self.left_hip][1:]

        left_shoulder_angle_z = get_angle(left_shoulder, [0., 0., 1.])
        right_shoulder_angle_z = get_angle(right_shoulder, [0., 0., 1.])
        left_forearm_angle_z = get_angle(left_forearm, [0., 0., 1.])
        right_forearm_angle_z = get_angle(right_forearm, [0., 0., 1.])
        left_hip_angle_z = get_angle(left_hip, [0., 0., 1.])
        right_hip_angle_z = get_angle(right_hip, [0., 0., 1.])

        return {
            'left_shoulder_angle_z': left_shoulder_angle_z,
            'right_shoulder_angle_z': right_shoulder_angle_z,
            'left_forearm_angle_z': left_forearm_angle_z,
            'right_forearm_angle_z': right_forearm_angle_z,
            'left_hip_angle_z': left_hip_angle_z,
            'right_hip_angle_z': right_hip_angle_z,
        }
        # return {
        #     'left_elbow': get_angle(left_shoulder, left_forearm),
        #     'right_elbow': get_angle(right_shoulder, right_forearm),
        #     'left_shoulder_hip': get_angle(left_shoulder, left_hip),
        #     'right_shoulder_hip': get_angle(right_shoulder, right_hip),
        #     'shoulders': get_angle(right_shoulder, left_shoulder),
        #     'hips': get_angle(right_hip, left_hip),
        # }

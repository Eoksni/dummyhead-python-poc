import numpy as np
import matplotlib.pyplot as plt
import attr
import time
import matplotlib


class DppGraph3:
    def __init__(self, config, title):
        self.plot_window = config['plot_window']
        self.x_var = np.array(np.zeros([self.plot_window]))
        self.y_var = np.array(np.zeros([self.plot_window]))
        self.z_var = np.array(np.zeros([self.plot_window]))
        self.fig, self.ax = plt.subplots()
        self.ax.set_title(title)
        self.ax.set_ylim(bottom=0, top=360)
        self.xline, = self.ax.plot(self.x_var, label='x')
        self.yline, = self.ax.plot(self.y_var, label='y')
        self.zline, = self.ax.plot(self.z_var, label='z')
        self.leg = self.ax.legend(loc='upper left', frameon=False)

    def append(self, data):
        self.x_var = np.append(self.x_var, data[0])
        self.y_var = np.append(self.y_var, data[1])
        self.z_var = np.append(self.z_var, data[2])
        self.x_var = self.x_var[1:self.plot_window+1]
        self.y_var = self.y_var[1:self.plot_window+1]
        self.z_var = self.z_var[1:self.plot_window+1]
        self.xline.set_ydata(self.x_var)
        self.yline.set_ydata(self.y_var)
        self.zline.set_ydata(self.z_var)
        # self.ax.relim()
        self.ax.autoscale_view()
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

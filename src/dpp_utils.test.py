import numpy as np
import dpp_utils as sut

input_data = b'209.35,173.60,193.11,170.36,103.85,164.22,187.36,175.78,184.92,175.65,152.92,178.69,185.67,179.03,181.67,179.07,173.99,180.09\r\n'
expected = [
    (209.35, 173.60, 193.11),
    (170.36, 103.85, 164.22),
    (187.36, 175.78, 184.92),
    (175.65, 152.92, 178.69),
    (185.67, 179.03, 181.67),
    (179.07, 173.99, 180.09)
]
actual = sut.raw2model(input_data)
print(np.array_equal(expected, actual))  # should return True

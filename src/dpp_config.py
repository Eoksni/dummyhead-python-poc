from dpp_logging import getLogger
import os
from dotenv import load_dotenv

log = getLogger(__name__)


def setup():
    load_dotenv()  # take environment variables from .env.

    # config values must be read-only
    config = {
        'port': os.getenv('DPP_PORT'),
        'plot_window': int(os.getenv('DPP_PLOT_WINDOW')),
        'number_of_sensors': int(os.getenv('DPP_NUMBER_OF_SENSORS')),
        'sensor_right_shoulder': int(os.getenv('DPP_SENSOR_RIGHT_SHOULDER')),
        'sensor_left_shoulder': int(os.getenv('DPP_SENSOR_LEFT_SHOULDER')),
        'sensor_right_forearm': int(os.getenv('DPP_SENSOR_RIGHT_FOREARM')),
        'sensor_left_forearm': int(os.getenv('DPP_SENSOR_LEFT_FOREARM')),
        'sensor_right_hip': int(os.getenv('DPP_SENSOR_RIGHT_HIP')),
        'sensor_left_hip': int(os.getenv('DPP_SENSOR_LEFT_HIP')),
        'calibrate_window': int(os.getenv('DPP_CALIBRATE_WINDOW')),
        'calibrate_initial_delay': int(os.getenv('DPP_CALIBRATE_INITIAL_DELAY')),
        'calibrate_delay': int(os.getenv('DPP_CALIBRATE_DELAY')),
        'calibrate_pass_through': bool(os.getenv('DPP_CALIBRATE_PASS_THROUGH')),
        'detector_no_movement_window': int(os.getenv('DPP_DETECTOR_NO_MOVEMENT_WINDOW')),
        'detector_no_movement_threshold_in': int(os.getenv('DPP_DETECTOR_NO_MOVEMENT_THRESHOLD_IN')),
        'detector_no_movement_threshold_out': int(os.getenv('DPP_DETECTOR_NO_MOVEMENT_THRESHOLD_OUT')),
        'data_output_dir': str(os.getenv('DPP_DATA_OUTPUT_DIR')),
        'data_model_dir': str(os.getenv('DPP_DATA_MODEL_DIR')),
        'collect_label': str(os.getenv('DPP_COLLECT_LABEL')),
        'collect_prewindow': int(os.getenv('DPP_COLLECT_PREWINDOW'))
    }

    log.debug(f'Config: {config}')

    return config

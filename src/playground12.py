import numpy as np
from vispy import app
from vispy import gloo

c = app.Canvas(keys='interactive')

#################

vertex = """
attribute vec3 a_position;
void main (void)
{
    gl_Position = vec4(a_position, 1.0);
}
"""

fragment = """
void main()
{
	float z = gl_FragCoord.z;
	float z_scaled = (z + 1.0) / 2.0;
	float g = z_scaled * 255.0;

    gl_FragColor = vec4(0.0, z_scaled, 0.0, 1.0);
}
"""

program = gloo.Program(vertex, fragment)

program['a_position'] = np.c_[
    np.array([-1, 0]),
    np.array([-1, 1]),
    np.array([-1, 1])
].astype(np.float32)

#################

vertexMesh = """
attribute vec3 mesh;
void main (void)
{
    gl_Position = vec4(mesh, 1.0);
}
"""

fragmentMesh = """
void main()
{
	float z = gl_FragCoord.z;
	float z_scaled = (z + 1.0) / 2.0;
	float g = z_scaled * 255.0;

    gl_FragColor = vec4(0.0, z_scaled, 0.0, 1.0);
}
"""

programMesh = gloo.Program(vertexMesh, fragmentMesh)

nx = 10
ny = nx
nz = ny
x = np.linspace(-1, 1, nx)
y = np.linspace(-1, 1, ny)
z = np.linspace(-1, 1, nz)

xv, yv, zv = np.meshgrid(x, y, z)

programMesh['mesh'] = np.c_[
    xv.flatten(),
    yv.flatten(),
    zv.flatten()
].astype(np.float32)


@ c.connect
def on_resize(event):
    gloo.set_viewport(0, 0, *event.size)


@ c.connect
def on_draw(event):
    gloo.clear((1, 1, 1, 1))
    program.draw('line_strip')
    programMesh.draw('line_strip')


c.show()
app.run()

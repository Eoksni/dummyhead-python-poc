#include "I2Cdev.h"
#include "MPU6050_6Axis_MotionApps_V6_12.h"
#include "Wire.h"

#define NBR_OF_SENSORS    6

MPU6050 mpu1(0x68);
/*MPU6050 mpu2(0x68);
MPU6050 mpu3(0x68);
MPU6050 mpu4(0x68);
MPU6050 mpu5(0x68);
MPU6050 mpu6(0x68);*/

//MPU6050 sensors[NBR_OF_SENSORS] = {mpu1,mpu2,mpu3,mpu4,mpu5,mpu6};


float angleX = 0;
float angleY = 0;
float angleZ = 0;

float qW,qX,qY,qZ = 0;

int nbrOfSensor = 6;

void setup() {

  Wire.begin();
  Wire.setClock(400000);
  Serial.begin(115200);
  

  //mpu1.initialize();
  //initDMP(&mpu1);
  
  for(int i = 0; i < nbrOfSensor; i++)
  {
    Wire.beginTransmission(0x70);
    Wire.write(1 << i);
    Wire.endTransmission(); 
    //delay(10);
    
    mpu1.initialize();
    initDMP();
    //delay(100);
  }

  
  
}

float arr[6][4];

void loop() {

  for(int i = 0;i < nbrOfSensor; i++)
  {
    Wire.beginTransmission(0x70);
    Wire.write(1 << i);
    Wire.endTransmission(); 
//    delay(50);
    getAngles();
    //arr[i][0] = angleX + 180;
    //arr[i][1] = angleY + 180;
    //arr[i][2] = angleZ + 180;
    arr[i][0] = qW;
    arr[i][1] = qX;
    arr[i][2] = qY;
    arr[i][3] = qZ;
    //delay(10);
  }

  for(int i = 0; i<nbrOfSensor; i++)
  {
    if(i>0) Serial.print(',');
    Serial.print(arr[i][0]);
    Serial.print(',');
    Serial.print(arr[i][1]);
    Serial.print(',');
    Serial.print(arr[i][2]);
    Serial.print(',');
    Serial.print(arr[i][3]);
  }
  Serial.println("");
  
  /*getAngles();
  Serial.print(angleX); Serial.print(',');
  Serial.print(angleY); Serial.print(',');
  Serial.println(angleZ);
  */
  delay(20);
}
// НУЖНЫЕ ПЕРЕМЕННЫЕ
const float toDeg = 180.0 / M_PI;
uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer
Quaternion q;           // [w, x, y, z]         quaternion container
VectorFloat gravity;    // [x, y, z]            gravity vector
float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector
// инициализация
void initDMP() {
  devStatus = mpu1.dmpInitialize();
  mpu1.setDMPEnabled(true);
  mpuIntStatus = mpu1.getIntStatus();
  packetSize = mpu1.dmpGetFIFOPacketSize();
}
// получение углов в angleX, angleY, angleZ
void getAngles() {
  if (mpu1.dmpGetCurrentFIFOPacket(fifoBuffer)) {
    mpu1.dmpGetQuaternion(&q, fifoBuffer);
    //mpu1.dmpGetGravity(&gravity, &q);
    //mpu1.dmpGetYawPitchRoll(ypr, &q, &gravity);

    qW = q.w;
    qX = q.x;
    qY = q.y;
    qZ = q.z;
    
    //angleX = ypr[2] * toDeg;
    //angleY = ypr[1] * toDeg;
    //angleZ = ypr[0] * toDeg;
  }
}
